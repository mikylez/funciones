<?php
require('conexion.php');
$id = $_GET["id"];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>Seleccionar Fecha</title>
    
</head>
<body>
<h3>Seleccioné Fecha</h3>
<br>
<div class="container pt-5">
    <div class="row">
<?php 
    $sql = "SELECT * FROM nivelsesiones WHERE id_evento ='".$id."'";
    $result = $con->query($sql);
    $row = $result->fetch_assoc();
    $busqueda = json_decode($row["Pais_Provincia"], TRUE);
    $idAforo=[];
    $idSesion=[];
    $fecha = [];
    $date1 = "";
    $date2 = "";
    $count = 0;
    if (isset($busqueda["@attributes"]))
    {   //solo hay 1 provincia 
        //echo("solo 1 provincia");
        //echo("<br>provincia: ".$busqueda["@attributes"]["Nombre"]);
        if (isset($busqueda["Poblacion"]["Recinto"]["Evento"]["Sesion"]["@attributes"]))
        {//solo una sesion
            //echo("<br>solo una sesion");
            //echo("<br>".$busqueda["Poblacion"]["Recinto"]["Evento"]["Sesion"]["@attributes"]["Desc3"]);
            $date = new DateTime($busqueda["Poblacion"]["Recinto"]["Evento"]["Sesion"]["@attributes"]["Desc3"]);
            //echo("<br>".$date->format('Y-m-d H:i:s')." ".$busqueda["@attributes"]["Nombre"]);
                echo"<div class='card shadow col-md-3 pt-5'>";
                echo"<a href='seleccionButacas.php?idSesion=".$busqueda["Poblacion"]["Recinto"]["Evento"]["Sesion"]["@attributes"]["IdSesion"]."'>".$date->format('Y-m-d H:i:s')." ".$busqueda["@attributes"]["Nombre"]."</a>";
                $disponibilidad=(int)$busqueda["Poblacion"]["Recinto"]["Evento"]["Sesion"]["@attributes"]["AforoMax"]-
                (int)$busqueda["Poblacion"]["Recinto"]["Evento"]["Sesion"]["@attributes"]["AforoOcupado"]; 
                echo"<p>Disponible :".$disponibilidad."</p>";
                echo"</div>";
        }
        else{//mas de una sesion
            //echo("<br>mas de una sesion");
            foreach ($busqueda["Poblacion"]["Recinto"]["Evento"]["Sesion"] as $i) {
               //echo("<br>".$i["@attributes"]["Desc3"]);
               $date = new DateTime($i["@attributes"]["Desc3"]);
               //echo("<br>".$date->format('Y-m-d')." ".$busqueda["@attributes"]["Nombre"]);
                    if($count == 0){
                    echo"<div class='card shadow col-md-3 pt-5'>";
                    //echo"<a href='#'>".$date->format('Y-m-d H:i:s')." ".$busqueda["@attributes"]["Nombre"]."</a>";
                    echo"<a href='seleccionButacas.php?idSesion=".$i["@attributes"]["IdSesion"]."'>".$date->format('Y-m-d H:i:s')." ".$busqueda["@attributes"]["Nombre"]."</a>";
                    $disponibilidad=(int)$i["@attributes"]["AforoMax"]-
                    (int)$i["@attributes"]["AforoOcupado"]; 
                    echo"<p>Disponible :".$disponibilidad."</p>";
                    //echo"</div>";
                    $date1=$date->format('Y-m-d'); 
                    }
                    else{
                        //foreach ($busqueda["Poblacion"]["Recinto"]["Evento"]["Sesion"] as $m) {
                            $date2 = new DateTime($i["@attributes"]["Desc3"]);
                            if($date2->format('Y-m-d')==$date1)
                            {
                                //echo"<a href='#'>".$date2->format('Y-m-d H:i:s')." ".$busqueda["@attributes"]["Nombre"]."</a>";
                                echo"<a href='seleccionButacas.php?idSesion=".$i["@attributes"]["IdSesion"]."'>".$date->format('Y-m-d H:i:s')." ".$busqueda["@attributes"]["Nombre"]."</a>";
                                $disponibilidad=(int)$i["@attributes"]["AforoMax"]-
                                (int)$i["@attributes"]["AforoOcupado"]; 
                                echo"<p>Disponible :".$disponibilidad."</p>";
                            }
                            else
                            {   
                                echo"</div>";
                                echo"<div class='card shadow col-md-3 pt-5'>";
                                //echo"<a href='#'>".$date2->format('Y-m-d H:i:s')." ".$busqueda["@attributes"]["Nombre"]."</a>";
                                echo"<a href='seleccionButacas.php?idSesion=".$i["@attributes"]["IdSesion"]."'>".$date->format('Y-m-d H:i:s')." ".$busqueda["@attributes"]["Nombre"]."</a>";
                                $disponibilidad=(int)$i["@attributes"]["AforoMax"]-
                                (int)$i["@attributes"]["AforoOcupado"]; 
                                echo"<p>Disponible :".$disponibilidad."</p>";
                                $date1=$date2->format('Y-m-d');
                            }

                        //}
                    }
                $count++;
            }
            echo"</div>";
        }
    }
    else
    {//hay mas de una provincia
        //echo("hay mas de 1 provincia");
        foreach ($busqueda as $i) 
        {   
            //echo("<br>provincia: ".$i["@attributes"]["Nombre"]);
            if (isset($i["Poblacion"]["Recinto"]["Evento"]["Sesion"]["@attributes"]))
            {//solo una sesion
            //echo("<br>solo una sesion");
            //echo("<br>".$busqueda["Poblacion"]["Recinto"]["Evento"]["Sesion"]["@attributes"]["Desc3"]);
            //$date = new DateTime($i["Poblacion"]["Recinto"]["Evento"]["Sesion"]["@attributes"]["Desc3"]);
            //echo("<br>".$date->format('Y-m-d H:i:s'));

            $date = new DateTime($i["Poblacion"]["Recinto"]["Evento"]["Sesion"]["@attributes"]["Desc3"]);
            //echo("<br>".$date->format('Y-m-d H:i:s')." ".$busqueda["@attributes"]["Nombre"]);
                echo"<div class='card shadow col-md-3 pt-5'>";
                //echo"<a href='#'>".$date->format('Y-m-d H:i:s')." ".$i["@attributes"]["Nombre"]."</a>";
                echo"<a href='seleccionButacas.php?idSesion=".$i["Poblacion"]["Recinto"]["Evento"]["Sesion"]["@attributes"]["IdSesion"]."'>".$date->format('Y-m-d H:i:s')." ".$i["@attributes"]["Nombre"]."</a>";
                $disponibilidad=(int)$i["Poblacion"]["Recinto"]["Evento"]["Sesion"]["@attributes"]["AforoMax"]-
                                (int)$i["Poblacion"]["Recinto"]["Evento"]["Sesion"]["@attributes"]["AforoOcupado"]; 
                echo"<p>Disponible :".$disponibilidad."</p>";
                echo"</div>";
            }
        else{//mas de una sesion
            //echo("<br>mas de una sesion");
            foreach ($i["Poblacion"]["Recinto"]["Evento"]["Sesion"] as $j) {
               //echo("<br>".$i["@attributes"]["Desc3"]);
               //$date = new DateTime($j["@attributes"]["Desc3"]);
               //echo("<br>".$date->format('Y-m-d H:i:s'))               
               $date = new DateTime($j["@attributes"]["Desc3"]);
               //echo("<br>".$date->format('Y-m-d')." ".$busqueda["@attributes"]["Nombre"]);
                    if($count == 0){
                    echo"<div class='card shadow col-md-3 pt-5'>";
                    //echo"<a href='#'>".$date->format('Y-m-d H:i:s')." ".$i["@attributes"]["Nombre"]."</a>";
                    echo"<a href='seleccionButacas.php?idSesion=".$j["@attributes"]["IdSesion"]."'>".$date->format('Y-m-d H:i:s')." ".$i["@attributes"]["Nombre"]."</a>";
                    echo"<p>Disponible :</p>";
                    //echo"</div>";
                    $date1=$date->format('Y-m-d'); 
                    }
                    else{
                        //foreach ($busqueda["Poblacion"]["Recinto"]["Evento"]["Sesion"] as $m) {
                            $date2 = new DateTime($i["@attributes"]["Desc3"]);
                            if($date2->format('Y-m-d')==$date1)
                            {
                                //echo"<br><a href='#'>".$date2->format('Y-m-d H:i:s')." ".$i["@attributes"]["Nombre"]."</a>";
                                echo"<a href='seleccionButacas.php?idSesion=".$j["@attributes"]["IdSesion"]."'>".$date2->format('Y-m-d H:i:s')." ".$i["@attributes"]["Nombre"]."</a>";
                                $disponibilidad=(int)$j["@attributes"]["AforoMax"]-
                                (int)$j["@attributes"]["AforoOcupado"]; 
                                echo"<p>Disponible :".$disponibilidad."</p>";
                            }
                            else
                            {   
                                echo"</div>";
                                echo"<div class='card shadow col-md-3 pt-5'>";
                                //echo"<a href='#'>".$date2->format('Y-m-d H:i:s')." ".$i["@attributes"]["Nombre"]."</a>";
                                echo"<a href='seleccionButacas.php?idSesion=".$j["@attributes"]["IdSesion"]."'>".$date2->format('Y-m-d H:i:s')." ".$i["@attributes"]["Nombre"]."</a>";
                                $disponibilidad=(int)$j["@attributes"]["AforoMax"]-
                                (int)$j["@attributes"]["AforoOcupado"]; 
                                echo"<p>Disponible :".$disponibilidad."</p>";
                                $date1=$date2->format('Y-m-d');
                            }

                        //}
                    }
                $count++;
            }
            echo"</div>";
        }
        }

    }
    //echo "<pre>"; 
    //print_r(json_decode($row["Pais_Provincia"], TRUE));
    //print_r($busqueda);
    //print_r($busqueda["Poblacion"]["Recinto"]["Evento"]["Sesion"]);
    mysqli_close($con);
?>
</div>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>